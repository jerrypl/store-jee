package listeners;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;
import javax.faces.event.ValueChangeListener;


public class DataChange implements ValueChangeListener {

    int counter;
    String key;

    public DataChange(String key) {
        this.key = key;
    }

    public DataChange() {
        this.key = "data";
    }

    /**
     *
     * @param valueChangeEvent
     * @throws AbortProcessingException
     */
    public void processValueChange(ValueChangeEvent valueChangeEvent) throws AbortProcessingException {
        String name;
        FacesContext context = FacesContext.getCurrentInstance();
        String clientId = valueChangeEvent.getComponent().getClientId();
        name = "" + valueChangeEvent.getNewValue();

        if (!name.equals("")) {
            if (context.getExternalContext().getSessionMap().containsKey(key)) {
                this.counter = (Integer) (context.getExternalContext().getSessionMap().get(key));
            }
            this.counter++;

            FacesMessage message = new FacesMessage("Stan licznika zmian " + key + ": " + counter);
            context.getExternalContext().getSessionMap().put(key, counter);
            context.addMessage(clientId, message);
        }
    }
}