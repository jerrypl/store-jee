package webLayer;

import businessLayer.BusinessLayerFacade;
import javafx.scene.chart.PieChart;
import listeners.DataChange;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.NumberConverter;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import java.util.Date;

@ManagedBean(name="ManagedProduct")
@RequestScoped
public class ManagedProduct implements ActionListener {

    @EJB
    private BusinessLayerFacade businessLayerFacade;
    private String name;
    private double price;
    private int promotion;
    private double priceGross;
    private String manufacturer;
    private DataModel items;
    private int state = 1;
    private Date created;

    private DataChange nameChange = new DataChange("name");
    private DataChange priceChange = new DataChange("price");

    public int getMin() {
        return 0;
    }
    public int getMax() {
        return 100;
    }

    public DataChange getNameChange() {
        return nameChange;
    }

    public void setNameChange(DataChange nameChange) {
        this.nameChange = nameChange;
    }

    public DataChange getPriceChange() {
        return priceChange;
    }

    public void setPriceChange(DataChange priceChange) {
        this.priceChange = priceChange;
    }

    public void promotionValidator(FacesContext context, UIComponent toValidate, Object value) {
        this.state = 1;
        int input = (Integer) value;
        if (input < getMin() || input > getMax()) {
            ((UIInput) toValidate).setValid(false);
            FacesMessage message = new FacesMessage("Wartość promocji powinna być z zakresu 0-100");
            context.addMessage(toValidate.getClientId(context), message);
            this.state = 0;
        }
    }

    public void priceValidator(FacesContext context, UIComponent toValidate, Object value) {
        this.state = 1;

        double price = (Double) value;
        if (price < 0) {
            ((UIInput) toValidate).setValid(false);
            FacesMessage message = new FacesMessage("Cena nie może być ujemna");
            context.addMessage(toValidate.getClientId(context), message);
            this.state = 0;
        }
    }

    public NumberConverter getNumberConvert() {
        numberConvert.setPattern("######.## zł");

        return numberConvert;
    }

    public void setNumberConvert(NumberConverter numberConvert) {
        this.numberConvert = numberConvert;
    }

    private NumberConverter numberConvert =new NumberConverter();

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public ManagedProduct() {

    }

    public BusinessLayerFacade getFacade() {
        return businessLayerFacade;
    }

    public void setFacade(BusinessLayerFacade facade) {
        this.businessLayerFacade = facade;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getPromotion() {
        return promotion;
    }

    public void setPromotion(int promotion) {
        this.promotion = promotion;

    }

    public double getPriceGross() {
        return price * 1.23;
    }

    public void setPriceGross(float priceGross) {
        this.priceGross = priceGross;
    }

    public void addProduct() {
        String[] data = {name, ""+price, ""+promotion, manufacturer};
        businessLayerFacade.createProduct(data, created);
        productData();
    }

    public void productData() {
        this.state = 1;
        String[] data = businessLayerFacade.productData();
        if (null != data) {
            this.name = data[0];
            this.price = Double.parseDouble(data[1]);
            this.promotion = Integer.parseInt(data[2]);
            this.priceGross = Double.parseDouble(data[3]);
            this.manufacturer = data[4];
            this.created.setTime(Long.parseLong(data[5]));
        } else {
            this.state = 0;
        }
    }

    public DataModel modelFactory() {
        return new ListDataModel(businessLayerFacade.items());
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public DataModel getItems() {
        if (null == items) {
            this.items = modelFactory();

        }

        return items;
    }

    public void processAction(ActionEvent actionEvent) throws AbortProcessingException {
        addProduct();
    }
}
