package businessLayer;
import javax.ejb.Stateless;
import businessLayer.entity.Product;

import java.util.ArrayList;
import java.util.Date;

@Stateless
public class BusinessLayerFacade {

    static long key = 0;
    private Product product;
    boolean isProductAdded = false;
    private ArrayList<Product> productsList = new ArrayList<Product>();

    public Product getProduct() {
        return product;
    }

    public ArrayList<Product> getProductsList() {
        return productsList;
    }

    public void setProductsList(ArrayList<Product> productsList) {
        this.productsList = productsList;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     *
     * @param productData
     */
    public void createProduct(String[] productData, Date created) {
        product = new Product();
        product.setName(productData[0]);
        product.setPrice(Float.parseFloat(productData[1]));
        product.setPromotion(Integer.parseInt(productData[2]));
        product.setManufacturer(productData[3]);
        product.setCreated(created);

        addProduct(product);
    }

    /**
     *
     * @return String[] {name, price, priceGross, promotion, manufacturer}
     */
    public String[] productData() {
        if (!isProductAdded) {
            return null;
        }

        Product product = productsList.get(productsList.size() - 1);
        String name = product.getName();
        String price = "" + product.getPrice();
        String priceGross = "" + product.getPriceGross();
        String promotion = "" + product.getPromotion();
        String manufacturer = product.getManufacturer();
        String created = ""+product.getCreated().getTime();

        String data[] = {name, price, promotion, priceGross, manufacturer, created};

        return data;
    }

    /**
     *
     * @return
     */
    public ArrayList<ArrayList<String>> items() {
        ArrayList<ArrayList<String>> data = new ArrayList<ArrayList<String>>();

        for (Product product : productsList) {
            ArrayList<String> singleProductRow = new ArrayList<String>();

//            singleProductRow.add(product.getId().toString());
            singleProductRow.add(product.getName());
            singleProductRow.add("" + product.getPrice());
            singleProductRow.add("" + product.getPriceGross());
            singleProductRow.add("" + product.getPromotion());
            singleProductRow.add(product.getManufacturer());
            singleProductRow.add(product.getCreated().toString());

            data.add(singleProductRow);
        }

        return data;
    }

    /**
     *
     * @param product
     */
    public void addProduct(Product product) {
        if (!productsList.contains(product)) {
            productsList.add(product);
            this.isProductAdded = true;
        } else {
            this.isProductAdded = false;
        }
    }

    public String[] getProductData() {
        String name = "brak produktu";
        String price = "brak produktu";
        String promotion = "brak produktu";
        String priceGross = "brak produktu";
        String manufacturer = "brak producenta";

        if (null != product) {
            name = product.getName();
            price= "" + product.getPrice();
            promotion = "" + product.getPromotion();
            priceGross = "" + product.getPriceGross();
            manufacturer = "" + product.getManufacturer();
        }
        String data[] = {name, price, promotion, priceGross, manufacturer};

        return data;
    }
}