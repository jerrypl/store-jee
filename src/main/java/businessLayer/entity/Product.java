package businessLayer.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    private String name;
    private float price;
    private int promotion;
    private String manufacturer;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date created;

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getPromotion() {
        return promotion;
    }

    public void setPromotion(int promotion) {
        this.promotion = promotion;
    }

    public float getPriceGross() {
        float priceGross = price * ( 1 - (float) promotion / 100);

        return priceGross;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);

        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (null == obj) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        final Product other = (Product) obj;

        if (Float.floatToIntBits(this.price) != Float.floatToIntBits(other.price)) {
            return false;
        }

        if (this.promotion != other.promotion) {
            return false;
        }

        if (!this.name.equals(other.name)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "businessLayer.entity.Product[ id=" + id + " ]";
    }

}